# -*- encoding: utf-8 -*-
import xlrd
from administration.models import *

LEVEL = "Nivel"

LEVEL_TABLE = {
	"N1" : "1",
	"N2" : "2",
	"N3" : "3",
	"x"   : "Activo"
}

#Este método toma el composition file y pobla todos los models Scenario con
#sus respectivas MitigationActions
def compositionParser(file_contents):

	book = xlrd.open_workbook(file_contents=file_contents)
	sheet = book.sheet_by_index(0)

	errorList = []
	isNew = False

	for colIndex in range(2,sheet.ncols):
		scenarioName = sheet.cell_value(3, colIndex)
		try:
			scenario = Scenario.objects.get(name = scenarioName)
		except Scenario.DoesNotExist:
			isNew = True
			#print("Escenario "+scenarioName+" no existe. Creandolo para composicion")
			# If the scenario doesn't exist, we create it
			scenario = Scenario(name=scenarioName)
			scenario.save()

			# Create the default style for the scenario
			style = ScenarioStyle.getDefaultScenarioStyle(scenario)
			style.save()

		scenarioParser(sheet, colIndex, scenario, isNew, errorList)
	return errorList

def scenarioParser(sheet, colIndex, scenario, isNew, errorList):

		for rowIndex in range(4,sheet.nrows):
			level = sheet.cell_value(rowIndex,colIndex)
			#Verificamos que la medida esta involucrada en el escenario
			name = sheet.cell_value(rowIndex, 1)

			if level == "":
				continue
			if level in LEVEL_TABLE:
				newLevel = LEVEL_TABLE[level]

				#check if scenario needs to remove an action that changed its level
				if not isNew:
					actions = scenario.actions.all().filter(name=name)
					if actions.exists() and actions[0].level != newLevel:
						#print("REMOVE: "+str(actions[0])+"\ņ")
						scenario.actions.remove(actions[0])
				# Try to add an action to the scenario. Stores an error list if it fails
				try:
					action = MitigationAction.objects.get(name__contains=name,level=newLevel)
					#print(u"\ņADD: "+str(action)+"\ņ")
					scenario.actions.add(action)

				except MitigationAction.DoesNotExist as e:
					#print("Medida: '" + name+ " (nivel " + LEVEL_TABLE[level]+")'  inexistente. Solicitada por escenario " + scenario.name)
					error = (name, level.encode("utf-8"), u"Medida no encontrada")
					if error not in errorList:
						errorList.append(error)
					continue
				except MitigationAction.MultipleObjectsReturned as e:
					#print "MultipleObjectsReturned on MitigationAction " + name + ": "
					continue
			else:
				errorList.append((name, level.encode("utf-8"), u"Nivel inválido"))
				if error not in errorList:
					errorList.append(error)
					


#Este método toma el archivo consolidado y pobla la base de datos con los
#modelos ScenarioValue
def consolidadoParser(file_contents):

	book = xlrd.open_workbook(file_contents=file_contents)
	sheet = book.sheet_by_index(0)

	for rowIndex in range(1,sheet.nrows):
		for colIndex in range(2,40):
			scenario_name = sheet.cell_value(rowIndex, 0)
			#Obtener el escenario
			try:
				scenario = Scenario.objects.get(name =scenario_name)
			except Scenario.DoesNotExist:
				# If the scenario doesn't exist, we create it
				scenario = Scenario(name=scenario_name)
				scenario.save()
				#print "Escenario " + scenario_name + " creado"

				# Create the default style for the scenario
				style = ScenarioStyle.getDefaultScenarioStyle(scenario)
				style.save()

			except Scenario.MultipleObjectsReturned as e:
				#print str(e) + " Scenario: " + scenario_name
				continue
			except Exception as e:
				#print str(e)
				continue
			#Obtener el sector
			sector_name = sheet.cell_value(rowIndex, 1)
			try:
				sector = Sector.objects.get(name = sector_name)
			except Sector.DoesNotExist as e:
				#print 'Error: "' + sector_name + '" not found'
				continue
			except Sector.MultipleObjectsReturned as e:
				#print e
				continue
			value = sheet.cell_value(rowIndex, colIndex)
			#Obtener el ScenarioValue correspondiente al Scenario, Sector y Año
			try:
				scenario_value = ScenarioValue.objects.get(
					scenario  = scenario,
					sector    = sector,
					year      = int(sheet.cell_value(0, colIndex))
					)
				scenario_value.value = value
			except ScenarioValue.DoesNotExist:
				scenario_value = ScenarioValue(
					scenario  = scenario,
					sector    = sector,
					year      = int(sheet.cell_value(0, colIndex)),
					value     = value
					)
			scenario_value.save()

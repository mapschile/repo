#encoding:utf-8
from django.forms import ModelForm, Textarea
from django import forms
from models import Sector, MitigationAction, Scenario, ScenarioStyle, FeatureToggle

class SectorForm(forms.ModelForm):
	class Meta:
		model 		= Sector
		fields		= '__all__'
		widgets		= {
			'description' : Textarea(attrs = {'cols' : 80, 'rows' : 7})
		}

class FeatureToggleForm(forms.ModelForm):
	class Meta:
		model 		= FeatureToggle
		fields 		= ['enabled']

class MeasureForm(forms.ModelForm):
	class Meta:
		model 		= MitigationAction
		fields		= '__all__'


class ScenarioForm(forms.ModelForm):
	class Meta:
		model 		= Scenario
		fields		= '__all__'

class ScenarioStyleForm(forms.ModelForm):
	class Meta:
		model 		= ScenarioStyle
		fields 		= ['color', 'marker', 'lineStyle', 'enabled', 'isVisible']

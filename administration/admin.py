from django.contrib import admin

from administration.models import Sector, MitigationAction, Scenario


admin.site.register(Sector)
admin.site.register(MitigationAction)
admin.site.register(Scenario)

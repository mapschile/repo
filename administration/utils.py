import re


def normalizeText(text):
	return re.sub(r'[^\w]', '', text).upper()

def MitigationBestMatch(name_string):
	''' Retorna la medida que tiene  '''
	words = name_string.split().filter(lambda (x) : len(x) > 3)
	actions_per_word = [MitigationAction.objects.filter(name__contains=word) for word in words]

	counter = {}

	for actionlist in actions_per_word:
		for action in actionlist:
			if not action in counter:
				counter[action] = 1
			else:
				counter[action] += 1


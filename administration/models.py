# -*- encoding: utf-8 -*-
import os

from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

AVAILABLE_LEVELS = (
		('act' , 'Activo'),
		('lvl1', 'Nivel 1'),
		('lvl2', 'Nivel 2'),
		('lvl3', 'Nivel 3'),
	)

LINE_TYPE = (
		('line', 'Linea'),
		('Dot','Punto'),
		('shortdash', 'Lineas cortas'),
		('ShortDashDotDot','Linea punto punto'),
		('LongDash','Linea Larga'),
	)

LEVEL = u"Nivel"

class Sector(models.Model):
	full_name 	= models.CharField("Nombre", max_length=100)
	name		= models.CharField("Nombre Corto", max_length=100, blank=True)
	nickname	= models.CharField("Nombre en Fichas", max_length=100)
	description = models.TextField("Descripción",blank=True)

	def __unicode__(self):
		return self.name

# Deben ser llave "name" + "level"
class MitigationActionRegistry(models.Model):
	title 			= models.CharField("nombre", max_length=200)
	sector 			= models.CharField("sector", max_length=200)
	# Django > 1.3 doesn't delete FileFields on Model delete. It must be deleted manually
	registry 		= models.FileField("ficha", upload_to='files')
	description		= models.TextField("descripción")
	modeling		= models.TextField("modelación")
	reduction		= models.TextField("reducción")
	costs			= models.TextField("costos")
	uncertainty		= models.TextField("incertidumbre")
	feasibility		= models.TextField("factibilidad")
	externalities	= models.TextField("externalidades")
	background		= models.TextField("antecedentes")


class Image(models.Model):
	# Django > 1.3 doesn't delete ImageField on Model delete. It must be deleted manually
	image 			= models.ImageField("imagen", upload_to='images')
	section 		= models.CharField("sección", max_length=50)
	registry 		= models.ForeignKey(MitigationActionRegistry)


class MitigationAction(models.Model):
	registry 	= models.ForeignKey(MitigationActionRegistry)
	level 		= models.CharField("nivel", max_length=20, choices=AVAILABLE_LEVELS)
	name 		= models.CharField("nombre", max_length=200)
	shortname 	= models.CharField("shortname", max_length=200, null=True)
	sector 		= models.ForeignKey(Sector)

	class Meta:
		ordering = ['name']

	@classmethod
	def parseTitle(cls, title):
		level = u"Activo"

		index = title.find(LEVEL)

		# We cut-off the level from the title
		if index >= 0 :
			name  = title[:index - 2]
			level = title[index + len(LEVEL) +1]
		else:
			name = title

		return {"level": level, "name": name}

	def __unicode__(self):
		return self.name


class Scenario(models.Model):
	name 		= models.CharField("nombre", max_length=200)
	actions 	= models.ManyToManyField(MitigationAction,
										 verbose_name="Medidas de mitigación")

	class Meta:
		ordering=['name']

	def __unicode__(self):
		return self.name

class FeatureToggle(models.Model):
	"""
	Modelo para activar y desactivar diferentes features de la aplicacion
	"""
	name 		= models.CharField("Nombre", max_length=100)
	enabled 	= models.BooleanField("Hablilitado", default=False)

class ScenarioStyle(models.Model):
	"""
	Los colores van en hexadecimal con #.
	Para los marcadores visit: http://api.highcharts.com/highcharts#plotOptions.spline.dashStyle
	Para los lineStyle visit: http://api.highcharts.com/highcharts#plotOptions.spline.marker.symbol
	"""
	scenario 	= models.ForeignKey(Scenario)
	color 		= models.CharField("Color", max_length=7)
	marker 		= models.BooleanField("Marcador", default=False)
	lineStyle 	= models.CharField("Estilo de linea", max_length=20, choices=LINE_TYPE)
	enabled		= models.BooleanField("Habilitado", default=True)
	isVisible 	= models.BooleanField("Es visible el escenario",default=True)

	@classmethod
	def getDefaultScenarioStyle(cls, scenario):
		style = ScenarioStyle(	scenario = scenario,
  								isVisible = True,
  								color = "#000000",
  								marker = False,
  								lineStyle = "line",
  								enabled = True)
		return style

	class Meta:
		ordering=['scenario__name']

	def __unicode__(self):
		return "%s, visible:%s, %s, marker:%s, %s, enabled:%s" % (self.scenario.name, self.isVisible, self.color,
			self.marker, self.lineStyle, self.enabled)

class ScenarioValue(models.Model):
	scenario 	= models.ForeignKey(Scenario)
	sector 		= models.ForeignKey(Sector)
	year 		= models.IntegerField()
	value 		= models.DecimalField(decimal_places=15, max_digits=30)

	class Meta:
		ordering = ['year']

class MitigationActionValue(models.Model):
	action 		= models.ForeignKey(MitigationAction)
	year 		= models.IntegerField()
	value 		= models.DecimalField(decimal_places=15, max_digits=30)

	class Meta:
		ordering = ['year']

	def __unicode__(self):
		return str((self.year, self.value))

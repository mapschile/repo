from django.contrib.auth.views import login
from django.http import HttpResponseRedirect

def custom_login(request, *args, **kwargs):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/admin/')
    else:
        return login(request, *args, **kwargs)
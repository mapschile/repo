from django.conf import settings
from django.conf.urls import patterns, url
from django.conf.urls.static import static
from administration import views

urlpatterns = patterns('administration.views',
    #url(r'^$', views.index, name='index'),
    url(r'^$', 'general.home', name='admin_index'),
    url(r'^home$', 'general.home', name='admin_home'),
#    url(r'^login$', 'login', name='admin_login'),
#    url(r'^logout$', 'logout', name='admin_logout'),

    url(r'^sector$', 'sector.home', name='admin_sector'),
    url(r'^sector/delete/(\d+)/$', 'sector.sectorDelete', name='admin_sector_delete'),
    url(r'^sector/edit/(\d+)/$', 'sector.sectorEdit', name='admin_sector_edit'),


    url(r'^measure$', 'measure.home', name='admin_measure'),
    url(r'^measure/add$', 'measure.measureAdd', name='admin_measure_add'),
    url(r'^measure/measureDetails$', 'measure.measureDetails', name='admin_measure_details'),
    url(r'^measure/delete/(\d+)/$', 'measure.measureDelete', name='admin_measure_delete'),
	url(r'^measure/file_handler$', 'measure.measureFileHandler', name="file_handler"),
    url(r'^measure/imageHandler$', 'measure.imageHandler', name="imageHandler"),
    url(r'^measure/shortname$', 'measure.shortname', name='admin_measure_shortname'),

    url(r'^scenario$', 'scenario.home', name='admin_scenario'),
    url(r'^scenario/add$', 'scenario.scenarioAdd', name='admin_scenario_add'),
    url(r'^scenario/configurate/(\d+)$', 'scenario.scenarioConfigurate', name='admin_scenario_configurate'),
    url(r'^scenario/delete/(\d+)/$', 'scenario.scenarioDelete', name='admin_scenario_delete'),
#    url(r'^scenario/edit$', 'scenarioEdit', name='admin_scenario_edit'),

    url(r'^feature/configurate/(\d+)$', 'features.configure', name='admin_feature_configurate'),
)

urlpatterns += patterns('',
	url(r'^media/(?P<path>.*)$','django.views.static.serve',{'document_root':settings.MEDIA_ROOT}),
)

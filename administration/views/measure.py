# -*- coding: utf-8 -*-

from django.shortcuts import render

import xlrd
import os
import logging

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext

# Usar name url para redireccionar
from django.core.urlresolvers import reverse

from django.conf import settings
from django.views import generic
from django.views.decorators.http import require_POST

from jfu.http import upload_receive, UploadResponse, JFUResponse

from administration.models import *
from administration.measure_parsers import *

# Formularios importados
from administration.forms import MeasureForm


def getOrderedMitigationActions():
  data = []
  sectors = Sector.objects.all()#.order_by('full_name')
  for sector in sectors:
    actions = MitigationAction.objects.filter(
        sector=sector).order_by('name','level')

    data.append({
        'sector': sector,
        'actions': actions})
  return data


def measureAdd(request):
  form = MeasureForm()
  data = {
    'form':form
  }
  return render_to_response('admin/measure-add.html', data, context_instance=RequestContext(request))


def measureDetails(request):
  if request.POST:
    if 'shortname' in request.FILES:
      found, not_found = shortname_parser(request.FILES['shortname'])
      data = {
        'processed' : True,
        'not_found_list' : not_found,
        'found_list' : found,
      }
      return render_to_response('admin/measure-details.html', data, context_instance=RequestContext(request))
  else:
    form = MeasureForm()
    data = {
      'not_found' : False,
      'form':form
    }
    return render_to_response('admin/measure-details.html', data, context_instance=RequestContext(request))


def shortname(request):
    if request.POST:
        if 'shortname' in request.FILES:
            found, not_found = shortname_parser(request.FILES['shortname'])
            data = {
                'processed' : True,
                'not_found_list' : not_found,
                'found_list' : found,
            }
            return render_to_response('admin/measure-shortname.html', data, context_instance=RequestContext(request))


    else:
        data = {
            'not_found' : False,
        }
        return render_to_response('admin/measure-shortname.html', data, context_instance=RequestContext(request))

# Medidas varias
def home(request):
  measurelist = getOrderedMitigationActions()
  return render_to_response('admin/measure.html', {'medidas': measurelist}, context_instance=RequestContext(request))


def measureEdit(request):
  return render_to_response('admin/home.html', context_instance=RequestContext(request))


def measureDelete(request, id):
  measurelist = getOrderedMitigationActions()
  try:
    action = MitigationAction.objects.get(pk=id)
  except MitigationAction.DoesNotExist:
    return HttpResponseRedirect(reverse('admin_measure'))

  # We delete all associated files FOR REAL
  images = Image.objects.filter(registry=action.registry)
  for image in images:
    image.image.delete()

  action.registry.registry.delete()
  action.registry.delete()
  action.delete()
  return HttpResponseRedirect(reverse('admin_measure'))


@require_POST
def imageHandler(request):
 
  imageFile = upload_receive(request)
  errorMsg=""
  name = unicode(imageFile._get_name())

  try:
    # Format: MitigationActionName;Section.png
    registryTitle, section = name.split(";")
    section = section.split(".")[0]

    actionRegistry = MitigationActionRegistry.objects.get(title__iexact=registryTitle)
    imageFile._set_name(str(hash(imageFile)) + ".png")

    # Check if the image exists and delete it
    images  = Image.objects.filter(section=section, registry=actionRegistry)

    if len(images) != 0:
      image = images[0]
      image.image.delete()
      image.image = imageFile
    else:
      # The image doesn't exist => Create it
      image = Image(image=imageFile, section=section, registry=actionRegistry)

    image.save()
  except MitigationActionRegistry.DoesNotExist as e:
    errorMsg = u"No se encontro la medida asociada"
  except ValueError as e:
    errorMsg = u"El archivo no tiene el formato adecuado: %s" % str(name)
  except Exception as e:
    errorMsg = u"Ha ocurrido un error grave en la aplicación: %s" % str(e)

  file_dict = {
      'name': name,
      'size': imageFile.size,
      'error': errorMsg
  }

  return UploadResponse(request, file_dict)


@require_POST
def measureFileHandler(request):
  
  uploadedFile = upload_receive(request)
  fileName  = uploadedFile._get_name()
  uploadedFile._set_name(str(hash(uploadedFile)) + ".xlsx")
  parsedFile = registryParser(uploadedFile)
  #images = image_parser(uploadedFile)

  registry = parsedFile["registry"]

  errorMsg = ""

  # We'll obtain the sector. If it doesn't exist we can't update the DB
  try:
    sector = Sector.objects.get(nickname=registry.sector)
  except Sector.DoesNotExist as e:
    error = 'For ' + registry.title + \
        ' sector ' + registry.sector + ' not found'
    #print error
    errorMsg = "No se encuentra el sector %s para la medida de mitigación"%(registry.sector)
  except Sector.MultipleObjectsReturned as e:
    #logging.error(e)
    errorMsg = "Se encontraron varios sectores para la medida de mitigación"
  except Exception as e:
    errorMsg = "Ha ocurrido un error buscando el sector %s para la medida de mitigación"%(registry.sector)

  # If we are here is because we'll update or create a MitigationActionRegistry. We'll delete the previous registry when updating the MitigationAction object
  registry.save()
  
  # We need to update the images everytime we update the registry
  #for image in images:
  #  Image(image=image, registry=registry).save()

  parsedTitle = MitigationAction.parseTitle(registry.title)

  action = getMitigationActionModel(
      parsedTitle['name'], parsedTitle['level'], sector, registry)

  # We need to save the modified or new action model
  action.save()
  
  saveMitigationActionValues(parsedFile["data"], action)

  file_dict = {
      'name': fileName,
      'size': uploadedFile.size,
      'error': errorMsg
  }
  return UploadResponse(request, file_dict)


def getMitigationActionModel(name, level, sector, registry):
  try:
    # First see if the mitigation action exist in order to update its registry
    action = MitigationAction.objects.get(
        sector=sector, name=name, level=level)

    #Delete the past registry first and update
    action.registry.registry.delete()
    action.registry.delete()
    action.registry = registry

  except MitigationAction.DoesNotExist:
    # If doesn't exist. We create it. Set the shortname equals to the name because the shortname is replaced by another file parser
    action = MitigationAction(registry=registry, sector=sector,
                             name=name, shortname = name, level=level)
  return action


def saveMitigationActionValues(data, action):
  for year, value in data:
    try:
      mitigationValue = MitigationActionValue.objects.get(
          action=action, year=year)
      mitigationValue.value = value
    except MitigationActionValue.DoesNotExist:
      mitigationValue = MitigationActionValue(
          action=action, year=year, value=value)
    except MitigationActionValue.MultipleObjectsReturned:
      error = "ERROR: Multiple objects for Mitigation Action %s (%d) and year %d."
      #print(error % (action.name, action.id, year))
      continue

    mitigationValue.save()

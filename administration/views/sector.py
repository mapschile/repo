# -*- encoding: utf-8 -*-

import os

from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext

# Usar name url para redireccionar
from django.core.urlresolvers import reverse

from django.conf import settings
from django.views import generic
from django.views.decorators.http import require_POST

from administration.models import *
from administration.parsers import *

# Formularios importados
from administration.forms import SectorForm

import logging
from measure import measureDelete

#Sectores varios
#Listado de sectores que existen
def home(request):
	listado = Sector.objects.all()
	return render_to_response('admin/sector.html', {'sectores' : listado}, context_instance=RequestContext(request))

def sectorEdit(request, id):
    sector = Sector.objects.get(pk=id)
    if request.POST:
        form = SectorForm(request.POST,instance = sector)
        form.save()
        return HttpResponseRedirect('/admin/sector')
    else:
        form = SectorForm(instance = sector)
        return render_to_response('admin/sector-edit.html', {'form':form}, context_instance=RequestContext(request))


def sectorDelete(request, id):
    listado = Sector.objects.all()
    sector = Sector.objects.get(pk = id)
    for measure in MitigationAction.objects.filter(sector=sector):
        measureDelete(request, measure.id)
    sector.delete()
    return render_to_response('admin/sector.html', {'sectores' : listado}, context_instance=RequestContext(request))

# -*- encoding: utf-8 -*-

from django.shortcuts import render

import xlrd
import os
import logging

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext

# Usar name url para redireccionar
from django.core.urlresolvers import reverse

from django.conf import settings
from django.views import generic
from django.views.decorators.http import require_POST

from jfu.http import upload_receive, UploadResponse, JFUResponse

from administration.models import *
from administration.forms import *
from administration.parsers import *


def configure(request, featurePk):
    try:
        feature     = FeatureToggle.objects.get(pk=featurePk)
    except FeatureToggle.DoesNotExist:
        raise Http404

    if not request.POST:
        form = FeatureToggleForm(
            initial={
                'enabled' : feature.enabled,
            })
        return render_to_response(
            'admin/feature-configurate.html',
            {'form':form, 'name':feature.name},
            context_instance=RequestContext(request)
        )
    form = FeatureToggleForm(request.POST)
    if form.is_valid():
        feature.enabled = form.cleaned_data['enabled']

        feature.save()

    return HttpResponseRedirect(reverse('admin_home'))



# -*- encoding: utf-8 -*-

from django.shortcuts import render

import xlrd
import os
import logging

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext

# Usar name url para redireccionar
from django.core.urlresolvers import reverse

from django.conf import settings
from django.views import generic
from django.views.decorators.http import require_POST

from jfu.http import upload_receive, UploadResponse, JFUResponse

from administration.models import *
from administration.parsers import *

def login(request):
    return render_to_response('admin/login.html', context_instance=RequestContext(request))

def logout(request):
    return render_to_response('admin/login.html', context_instance=RequestContext(request))

def home(request):
    features = FeatureToggle.objects.all()
    data = {
        'features' : [(feature.name, feature.pk) for feature in features]
    }
    return render_to_response('admin/home.html', data, context_instance=RequestContext(request))

# Panel de control
def setting(request):
    return home(request)

# -*- encoding: utf-8 -*-

from django.shortcuts import render

import logging

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext

# Usar name url para redireccionar
from django.core.urlresolvers import reverse

from django.conf import settings
from django.views import generic
from django.views.decorators.http import require_POST

from administration.models import *
from administration.parsers import *

from administration.forms import ScenarioForm, ScenarioStyleForm
from scenario_style import loadScenarioStyles

from administration.file_validators import *

# Escenarios varios
def home(request):
    listado = Scenario.objects.all()
    return render_to_response('admin/scenario.html', {'escenarios' : listado}, context_instance=RequestContext(request))

def scenarioAdd(request):
    if not request.POST:
        form = ScenarioForm()
        return render_to_response('admin/scenario-add.html', {'form':form}, context_instance=RequestContext(request))

    level_error_list = []

    if 'consolidado' in request.FILES:
        consolidado_file = request.FILES['consolidado']
        file_contents = consolidado_file.read()
        problemRows = validate_consolidado_file(file_contents)
        if len(problemRows) > 0:
            data = {
            'validation_errors' : True,
            'error_list' : problemRows,
            }

            return render_to_response('admin/scenario-add.html', data, context_instance=RequestContext(request))
        else:
            consolidadoParser(file_contents)

    if 'composition' in request.FILES:
        composition_file = request.FILES['composition']
        level_error_list = compositionParser(composition_file.read())

    form = ScenarioForm(request.POST, request.FILES)
    # Load the default scenario styles
    loadedScenarios = Scenario.objects.all()

    for scenario in loadedScenarios:
        loadScenarioStyles(scenario)

    if len(level_error_list) == 0:
        return HttpResponseRedirect('/admin/scenario')
    else:
        data = {
            'validation_errors' : False,
            'processed' : True,
            'level_error_list' : level_error_list,
        }
        return render_to_response('admin/scenario-add.html', data, context_instance=RequestContext(request))


def scenarioDelete(request, id):
    lst = Scenario.objects.all()
    try:
        scenario = Scenario.objects.get(pk = id)
        scenario.delete()
    except Scenario.DoesNotExist:
        pass
    return render_to_response('admin/scenario.html', {'escenarios' : lst}, context_instance=RequestContext(request))

def scenarioEdit(request):
    return render_to_response('admin/home.html', context_instance=RequestContext(request))

def scenarioConfigurate(request, id):

    try:
        scenarioStyle   = ScenarioStyle.objects.get(scenario__pk=id)

    except ScenarioStyle.DoesNotExist:
        pass

    if request.POST:
        form = ScenarioStyleForm(request.POST)

        if form.is_valid():
            scenarioStyle.isVisible = form.cleaned_data['isVisible']
            scenarioStyle.color     = form.cleaned_data['color']
            scenarioStyle.marker    = form.cleaned_data['marker']
            scenarioStyle.lineStyle = form.cleaned_data['lineStyle']
            scenarioStyle.enabled   = form.cleaned_data['enabled']

            scenarioStyle.save()

        return HttpResponseRedirect(reverse('admin_scenario'))

    else:
        form = ScenarioStyleForm(
            initial ={
                'scenario'  : scenarioStyle.scenario,
                'isVisible' : scenarioStyle.isVisible,
                'color'     : scenarioStyle.color,
                'marker'    : scenarioStyle.marker,
                'lineStyle' : scenarioStyle.lineStyle,
                'enabled'   : scenarioStyle.enabled,
            })
        return render_to_response('admin/scenario-configurate.html', {'form':form, 'name':scenarioStyle.scenario.name,}, context_instance=RequestContext(request))

#encoding:utf-8

import xlrd

def validate_measure_file(measure_file):
	return True
def validate_consolidado_file(file_contents):

    workbook    = xlrd.open_workbook(file_contents=file_contents)
    sheet       = workbook.sheet_by_index(0)

    ncols = sheet.ncols
    nrows = sheet.nrows

    #only validate that the numbers contain only digit characters

    problemRows = []

    for row in range(nrows):
        for col in range(2, ncols):
            try:
                float(sheet.cell_value(row,col))
            except ValueError:
                if row+1 not in problemRows:
                    problemRows.append(row+1)
    #print "Las siguientes filas del archivo contienen campos que no estan en formato decimal" + str(problemRows)
    return problemRows

def validate_composition_file(composition_file):
	return True

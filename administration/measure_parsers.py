# -*- encoding: utf-8 -*-
import json
import xlrd
import zipfile as zp
from django.core.files import File
from administration.models import MitigationActionRegistry, MitigationAction
from file_validators import *
import subprocess

def sub_sheet(sheet, ci, ri, cf, rf):
  """ Lee un bloque de la planilla."""
  return [[sheet.cell_value(r, c)
          for c in range(ci, cf + 1)]
          for r in range(ri, rf + 1)]

def image_parser(registry_file):
    """
    DEPRECADO
    """
    source = zp.ZipFile(registry_file, 'r')

    image_path = 'xl/media/'

    all_images = [name
            for name in source.namelist()
            if name.startswith(image_path)]

    image_names = [name
            for name in source.namelist()
            if name.endswith('png') or name.endswith('jpeg') or name.endswith('emf') or name.endswith('wmf')]
    images = []
    for image_name in image_names:

        image_path = source.extract(image_name, 'media/extracted')
        #tratando de convertir emf a png
        print image_path
        if image_path.endswith('emf') or image_path.endswith('wmf'):
            print subprocess.call(['unoconv','-f','png',image_path])

        #end tratando
        if image_path.endswith('emf'):
            image = File(open(image_path.replace('emf', 'png'), 'r'))
        elif image_path.endswith('wmf'):
            image = File(open(image_path.replace('wmf', 'png'), 'r'))
        else:
            image = File(open(image_path, 'r'))
        images.append(image)


    return images

def shortname_parser(shortname_file):
    book = xlrd.open_workbook(file_contents=shortname_file.read())
    sheet = book.sheet_by_index(0)

    not_found = []
    found = []

    for i in range(1, sheet.nrows):
        name = sheet.cell_value(i, 1)
        actions = MitigationAction.objects.filter(name__icontains=name)
        if actions.exists():
            for action in actions:
                action.shortname = sheet.cell_value(i, 2)
                action.save()
            found.append(name)
#            print("ENCONTRADO: "+name+" -> "+action.shortname)
        else:
            not_found.append(name)
 #           print("NO ENCONTRADO: "+name)

    return found, not_found

def registryParser(registry_file):
    workbook = xlrd.open_workbook(file_contents=registry_file.read())

    registry = registryInfoParser(workbook.sheet_by_name("Ficha"), registry_file)

    data = registryDataParser(workbook.sheet_by_index(1))

    return {"registry": registry, "data": data}


def registryInfoParser(sheet, registry_file):
    ''' parsea la ficha tecnica de una medida de mitigacion '''

    sector = sheet.cell_value(1, 2)
    title = sheet.cell_value(4, 2)

    description = json.dumps(descriptionParser(sub_sheet(sheet, 1, 4, 4, 9)))
    #print description

    modeling = json.dumps(modelingParser(sub_sheet(sheet, 1, 11, 4, 13)))

    reduction = json.dumps(reductionParser(sub_sheet(sheet, 1, 15, 4, 18)))
    

    costs = sub_sheet(sheet, 1, 20, 4, 37)
    # más formateo de decimales
    for i in [1, 8]:
        for j in range(1, 4):
            costs[i][j] = "{:.0%}".format(
                costs[i][j]) if costs[i][j] else costs[i][j]
    for i in [2, 3, 4, 5, 6, 9, 10, 11, 12, 13]:
        for j in range(1, 4):
            costs[i][j] = "{:,.0f}".format(
                costs[i][j]).replace(',','.') if costs[i][j] else costs[i][j]


    costs = json.dumps(costs)

    uncertainty = json.dumps(sub_sheet(sheet, 1, 35, 4, 37))

    # formateo de decimales
    feasibility = sub_sheet(sheet, 1, 39, 4, 39)
    feasibility[0][1] = int(feasibility[0][1])
    feasibility = json.dumps(feasibility)

    externalities = json.dumps(sub_sheet(sheet, 1, 41, 4, 41))
    background = json.dumps(sub_sheet(sheet, 1, 43, 4, 44))

    registry = MitigationActionRegistry(
        title=title,
        sector=sector,
        registry=registry_file,
        description=description,
        modeling=modeling,
        reduction=reduction,
        costs=costs,
        uncertainty=uncertainty,
        feasibility=feasibility,
        externalities=externalities,
        background=background)

    return registry


def descriptionParser(descriptionMatrix):
    '''parsea los campos de descripcion de la ficha de una medida de mitigacion'''

    # tabla de campos de la ficha tecnica y el nombre de la variable que se le
    # asocia en el template
    descriptionKeys = {
        u"Nombre": "name",
        u"Descripción general": "description",
        u"LB 2013": "lb2013",
        u"Año inicio": "initialYear",
        u"Año término": "endingYear",
        u"Región/es": "region",
        u"Descripción de la implementación (curva de adopción)": "implementation"}

    description = {}
    for row in descriptionMatrix:
        for i, value in enumerate(row):
            # checkea si el campo corresponde a alguno de los campos conocidos, en cuyo caso
            # la siguiente columna es el valor
            if value in descriptionKeys:
                description[descriptionKeys[value]] = row[i + 1]
    # formateo de decimales
    description['initialYear'] = int(description['initialYear'])
    description['endingYear'] = int(description['endingYear'])
    return description


def modelingParser(modelingMatrix):
    # parsea los campos de modelacion de la ficha de una medida de mitigacion

    # tabla de campos de la ficha tecnica y el nombre de la variable que se le
    # asocia en el template
    modelingKeys = {
        u"Principales supuestos": u"postulates",
        u"Elementos de costos": u"costs",
        u"Interrelación con otros sectores": u"interrelation"
    }

    modeling = {}
    # primero cortamos las ultimas dos columnas de cada fila
    matrix = [row[0:2] for row in modelingMatrix if row[1] != ""]
    for row in matrix:
        for i, value in enumerate(row):
            if value in modelingKeys:
                modeling[modelingKeys[value]] = row[i + 1]
    return modeling


def reductionParser(reductionMatrix):

    keys = {}
    reduction = {}
    a = [reductionMatrix[1][1], reductionMatrix[1][3]]
    b = [reductionMatrix[3][1], reductionMatrix[3][3]]
    # print(a)
    # print(b)
    reduction['evaluacion2030'] = ["{0:.2f}".format(x) if x else x for x in a]
    reduction['evaluacion2050'] = ["{0:.2f}".format(x) if x else x for x in b]

    return reduction


def registryDataParser(sheet):
    ############### DATOS PARA CREAR EL ESCENARIO PERSONALIZADO ###############

    data = []

    for colIndex in range(3, sheet.ncols):
        year = 2010 + colIndex
        value = sheet.cell_value(1, colIndex)

        if value == "":
            value = 0

        data.append((year, value))

    return data

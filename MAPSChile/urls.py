from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from MAPSChile import views

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', include('visualization.urls') ),

    url(r'^visualization/', include('visualization.urls')),
    url(r'^admin/', include('administration.urls')),
    #url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^login/$', 'administration.custom_login.custom_login', name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/login/'}, name="logout"),
    #url(r'^root/', include(admin.site.urls)),

    # Name resolution service for external Non-Linear Functions
    url(r'^nameResolution$', 'MAPSChile.views.nameResolution', name="nameResolution"),

    ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

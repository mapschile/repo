# -*- encoding: utf-8 -*-
import json
import difflib
import operator
import pprint
import urllib2
import urllib
from operator import attrgetter

from django.db.models import Q
from django.shortcuts import render
from django.core.validators import ValidationError
from django.core.urlresolvers import reverse
from django.utils.datastructures import MultiValueDictKeyError

from django.views.decorators.csrf import *

from django.shortcuts import render

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext

# Return with json
from django.core import serializers
from django.shortcuts import render
import urllib2
import urllib

from administration.models import MitigationAction

@csrf_exempt
def nameResolution(request):
  if request.POST:
    idList = json.loads(request.POST[u'ids'])

    responseData = dict()

    for measureId in idList:
      try:
        mitigationAction = MitigationAction.objects.get(pk=measureId)
        responseData[measureId] = mitigationAction.name
      except Exception:
        # If the MitigationAction does not exist or bad things happen we do not add the id to the list
        pass

    info = json.dumps(responseData)

    return HttpResponse(info, content_type='application/json')
  return HttpResponse(json.dumps("Debe ser llamado por POST"), content_type='application/json')

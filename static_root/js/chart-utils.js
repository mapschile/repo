
/*
Set the sector columns in the scenario chart to visible or invisible and
accumulate/subtract from the scenario total
*/

function updateBaseline(chart, sector, substract){
	$.ajax({
		data    : {'sector': sector},
		url     : '/visualization/data/baseline',
		type    : 'GET',
		success : function(data){
			var baseline = chart.get("Linea base 2013");
			var basedata = baseline.data;
			var serie = data['serie'];

			for(var i = 0; i < basedata.length; i++){
				if(substract){
					basedata[i].y -= parseFloat(serie[i]);
				}
				else{
					basedata[i].y += parseFloat(serie[i]);
				}
			}

			for(var i = 0; i < basedata.length; i++){
			}
			baseline.setData(basedata);
			}
		})
}


function setChartColumnsVisibility(chart, visibility){
  var serieTotal = chart.get("Total");
  var dataTotal = serieTotal.data;
  for(var i = 0; i < chart.series.length; i++){
  	// We just want to act on the sector columns
      if(chart.series[i].type == 'column'){
      	// If the visibility of the serie it's the equal to the desired
      	// visibility, do nothing
      	if(chart.series[i].visible == visibility)
      		continue;
          // Change the accumulated value of total
          dataTotal = accumulateValues(!visibility, dataTotal, chart.series[i].data);
          var sector = chart.series[i].name;
          updateBaseline(chart, sector, !visibility);
          // Change visibility of the serie
          chart.series[i].setVisible(visibility);
      }
  }
	serieTotal.setData(dataTotal);
}

var visibleCount ;

/*
Handles the activation or deactivation of a sector in the scenario chart.
*/
function handleSectorStateChange(event) {

  visibleCount = 0;
  chart.yAxis[0].setExtremes(null,null);
  for(var i = 0; i < chart.series.length; i++){
    // We just want to act on the sector columns
    if(chart.series[i].type == 'column'){
      if(chart.series[i].visible){
        visibleCount++;
      }
    }
  }

  if(visibleCount==1 && event.currentTarget.visible==true){
    setToZero(this.chart, ":)");
    console.log("zero!");
    //$('#buttonSectorVisibility').html('Mostrar sectores');
    return true;
  }

	var sector = event.currentTarget.legendGroup.element.firstChild.firstChild.innerHTML;
	updateBaseline(this.chart, sector, this.visible)

	// Obtain the "Total" serie from the chart
	var serieTotal = this.chart.get("Total");
	var dataTotal = serieTotal.data;
	dataTotal = accumulateValues(this.visible, dataTotal, this.data);
	serieTotal.setData(dataTotal);
}


function setToZero(chart, msg){
  var baseline = chart.get("Linea base 2013");
  var serieTotal = this.chart.get("Total");
  var minimum = Math.min(serieTotal.data.length, baseline.data.length);
  for (var i = minimum - 1; i >= 0; i--) {
    serieTotal.data[i].y = 0.0;
    baseline.data[i].y = 0.0;
  };
  chart.yAxis[0].setExtremes(-10,10);
  //console.log(msg);
}

function replace(dataArrayInput, dataArrayOutput){
  var minimum = Math.min(dataArrayInput.length, dataArrayOutput.length);
  for (var i = minimum - 1; i >= 0; i--) {
    dataArrayInput[i].y = dataArrayOutput[i].y;
  };
}


/*
Sum or substract the value serie.y to/from total.y depending on the desired visibility
*/
function accumulateValues(substract, total, serie){
	if(substract){
		for(var i = 0; i < total.length; i++){
			total[i].y -= serie[i].y;
		}
	}else{
		for(var i = 0; i < total.length; i++){
			total[i].y += serie[i].y;
		}
	}
	return total;
}

# -*- encoding: utf-8 -*-
import xlrd

#Encabezado fila 3
#Escenarios columna 2 a 9
#Medidas columna 1

from administration.models import Scenario, ScenarioValue, MitigationAction

def getScenariosDB():

  data = []
  scenarios = Scenario.objects.all()

  for scenario in scenarios:
    medidas = MitigationAction.objects.filter(scenario__pk=scenario.pk)

    data.append({
      'escenario' : scenario.name,
      'medidas' : medidas
      })



def getScenarios():
  location = "Composicion Escenarios.xlsx"
  workbook =xlrd.open_workbook(location)
  sheet = workbook.sheet_by_index(0)

  data = []
  for escenarioIndex in range(2,sheet.ncols):
    medidas = []
    for rowIndex in range(4,sheet.nrows):
      nivel = sheet.cell_value(rowIndex,escenarioIndex)
      if nivel == "x":
        nivel = "N1"
      if nivel != "":
        medidas.append(
          {
            'medida' : sheet.cell_value(rowIndex,1),
            'nivel' : nivel
          })
    data.append({
      'escenario' : sheet.cell_value(3,escenarioIndex),
      'medidas' : medidas
      })

  return data


def main():
  data = getScenarios()
  #print(data)


if __name__ == "__main__":
  main()

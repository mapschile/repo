# -*- encoding: utf-8 -*-

import xlrd
import json
import difflib
import operator
from itertools import groupby

from django.shortcuts import render
from django.core.validators import ValidationError
from django.utils.datastructures import MultiValueDictKeyError

from django.shortcuts import render

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext

#Return with json
from django.core import serializers

#from escenarios_parser import getScenarios
from visualization.utils import *
from administration.models import *

from scenario import scenarioDataGetterDB
from comparison import sectorDataGetterDB
from registry import registryDataGetterDB


BASELINE = u'Línea Base 2013 (PIB medio)'


# home del sitio web
def index(request):
  scenarios = getScenarios()
  data = {
    #'groupedActions': actions
    'scenariosList': scenarios,
    'personalized': str(FeatureToggle.objects.all()[0].enabled)
    }

  return render_to_response('visualization/index.html', data, context_instance=RequestContext(request))

def credits(request):
  return render_to_response( 'visualization/credits.html', {
        'scenariosList' :  getScenarios(),
        'personalized': str(FeatureToggle.objects.all()[0].enabled),
        },

    context_instance=RequestContext(request))


def imageViewer(request):
  images = Image.objects.all()
  return render_to_response('visualization/images.html', {'images':images}, context_instance= RequestContext(request))

def actionListViewer(request):

  sectors = Sector.objects.all()
  actionList = []
  for sector in sectors:
    #agrupamos medidas por nombre
    actiongroup = groupby(MitigationAction.objects.filter(sector=sector).order_by('name','level'), lambda action: action.name)
    #convertimos los iteradores a listas
    actiongroup = [(name, [action for action in group]) for name,group in actiongroup]
    #action list es una lista de tuplas (nombresector, lista medidas)
    actionList.append((sector, actiongroup))

  return render_to_response( 'visualization/actionlist.html', {
        'actionList'  : actionList,
        'scenariosList' :  getScenarios(),
        'personalized': str(FeatureToggle.objects.all()[0].enabled),
        },

    context_instance=RequestContext(request))

def baseline(request):
  if request.method == 'GET':
    base = Scenario.objects.get(name=BASELINE)
    sector = request.GET['sector']
    data = [float(val.value) for val in ScenarioValue.objects.filter(scenario=base, sector__full_name__contains=sector)]

    info = {
      'serie':data
    }

    return HttpResponse(json.dumps(info), content_type='application/json')

def dataSelector(request):
  chartName = "General"

  if 'sector' in request.GET:
    requestedData = request.GET['sector']

    # Verificamos si existe el id_sector cuando no es general
    if request.GET['sector'] != GENERAL:
      try:
        sector = Sector.objects.get(full_name = requestedData)
      except Sector.DoesNotExist:
        # El sector no existe en la base de datos
        info = "ERROR : Atributo no valido"
        return HttpResponse(info)

      chartName = sector.full_name
    data = sectorDataGetterDB(requestedData)

  elif 'scenario' in request.GET:
    requestedData = request.GET['scenario']
    data = scenarioDataGetterDB(requestedData)

  # Si tenemos datos
  if data:
    years = range(2013,2051)
    info = json.dumps({ 'years' : years,
                        'value': data,
                        'name' : chartName})

    return HttpResponse(info, content_type='application/json')

  else:
    info = "ERROR : Información no encontrada"
    return HttpResponse(info)

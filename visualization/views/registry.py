# -*- encoding: utf-8 -*-
from django.shortcuts import render

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext
from math import log


# Return with json
from django.core import serializers

#from escenarios_parser import getScenarios
from visualization.utils import *
from administration.models import *

import json


def registry_viewer(request):
  ''' recibe por GET el id de la medida a desplegar'''

  try:
    pk = request.GET['id']

    action = MitigationAction.objects.get(pk=pk)
    actionRegistry = action.registry
    # We need to convert to json dict because the DB stores it as string
    data = {
        'sector': actionRegistry.sector,
        'description': json.loads(actionRegistry.description),
        'modelacion': json.loads(actionRegistry.modeling),
        'reduccion': json.loads(actionRegistry.reduction),
        'costos': json.loads(actionRegistry.costs),
        'incertidumbre': json.loads(actionRegistry.uncertainty),
        'factibilidad': json.loads(actionRegistry.feasibility),
        'externalidades': json.loads(actionRegistry.externalities),
        'antecedentes': json.loads(actionRegistry.background)
    }
    data['personalized'] = str(FeatureToggle.objects.all()[0].enabled)
    data['id'] = pk
    data['scenariosList'] = getScenarios()
    data['action'] = action
    data['images'] = Image.objects.filter(registry=actionRegistry)
    #print data['externalidades']

  except MitigationAction.DoesNotExist as e:
    raise Http404('No existe la medida de mitigación solicitada.')
  except ValueError as e:
    raise Http404('No existe la medida de mitigación solicitada.')
  except Exception as e:
    raise Http404('No existe la medida de mitigación solicitada.')
  return render_to_response('visualization/registry.html', data, context_instance=RequestContext(request))

def actionGraphComparison(request):

  try:
    pk = request.GET['id']
    action = MitigationAction.objects.get(pk=pk)
    dataLength = int(request.GET['dataLength'])
    graphData = registryDataGetterDB(action, dataLength)

    info = json.dumps({'value': graphData,
                       'name': "mitigationActionData"})

    return HttpResponse(info, content_type='application/json')

  except MitigationAction.DoesNotExist as e:
    print e
  except ValueError as e:
    print e
  except Exception as e:
    print e


def actionGraphReduction(request):

  try:
    pk = request.GET['id']
    dataLength = int(request.GET['dataLength'])

    action = MitigationAction.objects.get(pk=pk)
    mitigationActionValue = MitigationActionValue.objects.filter(
        action_id=action)
    mitigationActionValues = [float(element.value)
                              for element in mitigationActionValue]

    mitigationActionValues = mitigationActionValues[:dataLength]

    graphData = {
        'name': action.shortname,
        'type': 'area',
        'data': mitigationActionValues
    }
    info = json.dumps({'value': [graphData],
                       'name': "mitigationActionData"})

    return HttpResponse(info, content_type='application/json')

  except MitigationAction.DoesNotExist as e:
    print e
  except ValueError as e:
    print e
  except Exception as e:
    print e


def registryDataGetterDB(actionId, dataLength):

  data = []
  baselineValues = getBaseline()[:dataLength]
  mitigationAppliedValues = []

  mitigationActionValue = MitigationActionValue.objects.filter(
      action_id=actionId)
  mitigationActionValues = [float(element.value)
                            for element in mitigationActionValue][:dataLength]

  # We substract the mitigation action values to the base line
  mitigationAppliedValues = [
      v1 - v2 for v1, v2 in zip(baselineValues, mitigationActionValues)]

  data.append({
      'name': "Linea base 2013",
      'type': 'spline',
      'data': baselineValues})
  data.append({
      'name': mitigationActionValue[0].action.name,
      'type': 'spline',
      'data': mitigationAppliedValues
  })
  return data

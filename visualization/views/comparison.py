# -*- encoding: utf-8 -*-

import xlrd
import json
import difflib

from operator import attrgetter

from django.shortcuts import render
from django.core.validators import ValidationError
from django.utils.datastructures import MultiValueDictKeyError


from django.shortcuts import render

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext


#Return with json
from django.core import serializers

#from escenarios_parser import getScenarios
from visualization.utils import *

from administration.models import *

# Read Mitigation actions
def home(request):
	scenarios = getScenarios()

	data={
		'EmptyScenarioList':True,
		'personalized': str(FeatureToggle.objects.all()[0].enabled),
	}

	if len(scenarios) < 2:
		return render_to_response('visualization/comparison.html',data, context_instance=RequestContext(request))

	sectors = Sector.objects.all()
	data = {
		#'groupedActions': actions
		'scenariosList': scenarios,
		'sectors' : sectors,
		'personalized': str(FeatureToggle.objects.all()[0].enabled)
		}

	return render_to_response('visualization/comparison.html', data, context_instance=RequestContext(request))


def sectorDataGetterDB(sector_name):
	data = []
	scenarios = getScenarios()
	dataLength = 18 #there're 18 values up to 2030, 30 up to 2050

	if sector_name == GENERAL:
		#si me piden el caso general, debo retornar la suma de todos los sectores
		general_data = {} #diccionario para ir sumando los valores por sector
		sectors = Sector.objects.all()

		for sector in sectors:
			for scenario in scenarios:
				scenario_name = scenario.name
				values = ScenarioValue.objects.filter(scenario__pk=scenario.pk,sector__pk=sector.pk)
				data_values = [float(v.value) for v in values]
				if scenario_name in general_data:
					old_values = general_data[scenario_name]
					general_data[scenario_name] = [x + y for x,y in zip(old_values, data_values)]
				else:
					general_data[scenario_name] = data_values[:dataLength]

		for scenario in scenarios:
			style = ScenarioStyle.objects.get(scenario = scenario)
			data.append({
				'type' : 'spline',
				'name' : scenario.name,
				'data' : general_data[scenario.name],
				'dashStyle' : style.lineStyle,
				'color' : style.color,
				'marker' : {
					'enabled' : style.marker,
					'symbol' : 'square'
				},
				'visible' : style.enabled
			})

	#me piden un sector en particular
	else:
		try:
			baselineValues = getGroupedBaseline()[sector_name]
		except Exception, e:
			info = "ERROR: Sector name isn't in baseline sectors"
			print(info+": "+sector_name+". "+str(getGroupedBaseline()))
			return

		sector_id = Sector.objects.get(full_name=sector_name).id

		for scenario in scenarios:
			#para cada escenario retorno una lista de valores ordenados por year
			values = ScenarioValue.objects.filter(scenario__pk=scenario.pk,sector__pk=sector_id)
			values = [float(scenario_value.value) for scenario_value in values][:dataLength]

			style = ScenarioStyle.objects.get(scenario = scenario)

			# Enable the scenario only if it affects the sector
			if scenario.name != BASELINE and equalValues(values, baselineValues):
				visible = False
				#print("%s is equal to the baseline => %s"%(scenario.name, visible))
			else:
				visible = style.enabled


			data.append({
				'type' : 'spline',
				'name' : scenario.name,
				'data' : values,
				'dashStyle' : style.lineStyle,
				'color' : style.color,
				'marker' : {
					'enabled' : style.marker,
					'symbol' : 'square'
				},
				'visible' : visible
			})

	return data

# -*- encoding: utf-8 -*-
import json
import difflib
import operator
import pprint
import urllib2
import httplib2
import urllib
from operator import attrgetter

from django.db.models import Q
from django.shortcuts import render
from django.core.validators import ValidationError
from django.core.urlresolvers import reverse
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext

# Return with json
from django.core import serializers

#from escenarios_parser import getScenarios
from visualization.utils import *
from administration.models import *
from MAPSChile.settings import FUNCTION_URL

def scenario(request):
  """
  Loads the personalized scenario view.
  Needs to charge all the mitigation actions grouped by sectors and all their levels.
  Additionally it needs to pass the selected levels by the user.
  """

  try:
    enabled =  FeatureToggle.objects.get(name="Escenario Personalizado").enabled
  except FeatureToggle.DoesNotExist:
    FeatureToggle(name="Escenario Personalizado", enabled = False).save()
    enabled = False

  if not enabled:
    raise Http404

  scenario = personalizedScenario(request)

  sectors = Sector.objects.all()#.order_by('full_name')
  sectorMeasures = []
  selectedLevels = dict()

  for sector in sectors:
    actions = MitigationAction.objects.filter(sector=sector).filter(
        Q(level="Activo") | Q(level="1")).order_by('name')
    measures = []

    # Añade los niveles
    for action in actions:

      mitigations = MitigationAction.objects.filter(name=action.name).order_by('level')
      actionLevels = [mitigation.level for mitigation in mitigations]
      actionLevelsIDs = [(mitigation.level, mitigation.id) for mitigation in mitigations]

      activatedLevel = '0' if len(actionLevels) == 1 else 'inactivo'
      selectedId = action.id

      if action.name in scenario:

        selectedId = scenario[action.name]
        if selectedId in [mitigation.id for mitigation in mitigations]:
          activatedLevel = MitigationAction.objects.get(pk=selectedId).level
          selectedLevels[selectedId] = activatedLevel

      # Medida +  Niveles
      measures.append({
          'id': selectedId,
          'name': action.name,
          'shortname': action.shortname,
          'levels': actionLevelsIDs
      })

    # Sectores + Medidas
    sectorMeasures.append({
        'sector': sector,
        'actions': measures})

  data = {
      'scenariosList': getScenarios(),
      'personalized': str(FeatureToggle.objects.all()[0].enabled),
      'measures': sectorMeasures,
      'selectedLevels': json.dumps(selectedLevels)
  }



  return render_to_response('visualization/personalized-scenario.html', data, context_instance=RequestContext(request))


def personalizedScenario(request):
  if not ('personalizedScenario' in request.session):
    request.session['personalizedScenario'] = dict()
  return request.session['personalizedScenario']


def cleanPersonalizedScenario(request):
  request.session['personalizedScenario'] = dict()
  return HttpResponseRedirect(reverse('scenario_personalized'))


def saveScenario(request, scenario):
  request.session['personalizedScenario'] = scenario

# Llamada AJAX que cambia la selección de una de las medidas del escenario personalizado
# Recibe por get:
#   'id'    : id de la medida de nivel 1 o Activo para saber el nombre de la medida en cuestión
#   'level' : nivel al que se quiere cambiar la medida, en caso de querer desactivar envía 0


def updatePersonalizedScenario(request):
  try:
    requestedScenario = json.loads(request.GET['scenario'])
  except Exception as e:
    return HttpResponse("Error: Invalid format", content_type='application/json')

  scenario = personalizedScenario(request)

  for actionId in requestedScenario:
    # Obtenemos el nombre de la acción previa
    try:
      actionName = MitigationAction.objects.get(pk=actionId).name
    except MitigationAction.DoesNotExist:
      return HttpResponse("Error: Action ID invalid", content_type='application/json')

    # Eliminamos la acción previa
    if actionName in scenario:
      del scenario[actionName]

    level = requestedScenario[actionId]

    # Agregamos la nueva acción
    try:
      scenario[actionName] = MitigationAction.objects.get(name=actionName, level=level).id
    except MitigationAction.DoesNotExist:
      pass  # Caso donde desactivamos una medida

  saveScenario(request, scenario)

  data = personalizedScenario(request).values()
  data = urllib.urlencode({'scenario' : data})
  req = urllib2.Request(FUNCTION_URL,data)
  response = urllib2.urlopen(req)

  return HttpResponse(response, content_type='application/json')

def testPersonalizedScenario(request):
  if 'b' in request.session:
    request.session['b'] = 'FOOO'
  return HttpResponse(json.dumps(personalizedScenario(request)), content_type='application/json')

# -*- encoding: utf-8 -*-

import xlrd
import json
import difflib

from django.shortcuts import render
from django.core.validators import ValidationError
from django.utils.datastructures import MultiValueDictKeyError


from django.shortcuts import render

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext


# Return with json
from django.core import serializers

#from escenarios_parser import getScenarios
from visualization.utils import *
from administration.models import *

from personalized import personalizedScenario


def scenario_default(request):
  """
  Loads the first scenario in the DB
  """
  try:
    return home(request, getScenarios()[0].id)
  except IndexError:
    data = {
        'emptyScenarioTable': True,
        'personalized': str(FeatureToggle.objects.all()[0].enabled),
    }
    return render_to_response('visualization/scenario.html', data, context_instance=RequestContext(request))


def home(request, scenarioId):
  """
  Loads the scenario with the requested id
  """
  try:
    scenario = Scenario.objects.get(pk=scenarioId)

    data = {
        'id': scenario.id,
        'escenario': scenario.name,
        # This populates the dropdown scenario list from the base.html
        'scenariosList':  getScenarios(),
        'personalized': str(FeatureToggle.objects.all()[0].enabled)
    }

    return render_to_response('visualization/scenario.html', data, context_instance=RequestContext(request))
  except Scenario.DoesNotExist:
    data = {
        'scenarioNotFound': True,
    }
    return render_to_response('visualization/scenario.html', data, context_instance=RequestContext(request))


def scenarioDataGetterDB(scenarioId):

  BASELINE = u'Línea Base 2013 (PIB medio)'

  # The data array with all the values in the format of the highcharts chart
  data = []
  dataLength = 18 #there're 18 values up to 2030, 30 up to 2050

  # We need the baseline object in order to obtain their values
  scenarioBaseline = Scenario.objects.get(name=BASELINE).id

  # These will be used to hold the values across multiple sectors
  baselineValues = []
  totalValues = []

  for sector in Sector.objects.all().order_by('full_name'):

    scenarioValue = ScenarioValue.objects.filter(
        scenario_id=scenarioId, sector=sector)

    values = [float(element.value) for element in scenarioValue][:dataLength]

    # apilo la informacion
    data.append({
        'name': sector.full_name,
        'id': sector.full_name,
        'type': 'column',
        'data': values})

    # Obtain all the values for the baseline for a given sector
    scenarioBaselineValue = ScenarioValue.objects.filter(
        scenario_id=scenarioBaseline, sector=sector)

    arrayBaseLineValue = [float(element.value)
                          for element in scenarioBaselineValue]

    # We add the values of the baseline across all sectors
    if not baselineValues:
      baselineValues = arrayBaseLineValue[:dataLength]
    else:
      baselineValues = [
          v1 + v2 for v1, v2 in zip(baselineValues, arrayBaseLineValue)][:dataLength]

    # We add the values to the total line
    if not totalValues:
      totalValues = values[:dataLength]
    else:
      totalValues = [v1 + v2 for v1, v2 in zip(totalValues, values)][:dataLength]

  # Finally we append the total and baseline to the chart. Both will be
  # shown as splines over the bar plot
  data.append({
      'name': Scenario.objects.get(id=scenarioId).name,
      'id': "Total",
      'type': 'spline',
      'data': totalValues})

  data.append({
      'name': "Linea base 2013",
      'id': "Linea base 2013",
      'type': 'spline',
      'data': baselineValues})

  # RETORNAR
  # data: "name_sector, type_graphic, [values]"
  return data


def compositionData(request):
  id = request.GET["id"]
  scenario = Scenario.objects.get(id=id)
  actions = scenario.actions.all().order_by("sector",'name')
  mitigationActionList = []
  for action in actions:
    mitigationActionList.append(
        {
            "id": action.id,
            "medida": action.name,
            "shortname": action.shortname,
            "nivel": action.level,
            "sector": action.sector.full_name
        })

  try:
    info = json.dumps(mitigationActionList)
  except Exception as e:
    #print e
    return HttpResponse("error", content_type='application/json')
  return HttpResponse(info, content_type='application/json')

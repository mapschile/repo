import re
from django import template


register = template.Library()

@register.filter(name='clearString')
def clearString(data):
  return re.sub(r'[^\w]', '', data).lower()

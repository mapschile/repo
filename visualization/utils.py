# -*- coding: utf-8 -*-
import re
import collections
import pprint

from operator import attrgetter, eq

from administration.models import *

GENERAL = "General"
BASELINE = u'Línea Base 2013 (PIB medio)'

def getScenarios(onlyVisible=True):
  if onlyVisible:
    return map(attrgetter('scenario'), ScenarioStyle.objects.filter(isVisible=True))
  else:
    return Scenario.objects.all()

def getBaseline():

  # We need the baseline object in order to obtain their values
  scenarioBaseline = Scenario.objects.get(name=BASELINE).id

  # These will be used to hold the values across multiple sectors
  baselineValues = []

  for sector in Sector.objects.all():

    # Obtain all the values for the baseline for a given sector
    scenarioBaselineValue = ScenarioValue.objects.filter(
      scenario_id = scenarioBaseline, sector = sector)

    arrayBaseLineValue = [float(element.value) for element in scenarioBaselineValue]

    # We add the values of the baseline across all sectors
    if not baselineValues:
      baselineValues = arrayBaseLineValue
    else:
      baselineValues = [v1 + v2 for v1,v2 in zip(baselineValues, arrayBaseLineValue)]

  return baselineValues

def getGroupedBaseline():
  # We need the baseline object in order to obtain their values
  scenarioBaseline = Scenario.objects.get(name=BASELINE).id

  # These will be used to hold the values across multiple sectors
  baselineValues = dict()

  for sector in Sector.objects.all():

    # Obtain all the values for the baseline for a given sector
    scenarioBaselineValue = ScenarioValue.objects.filter(
      scenario_id = scenarioBaseline, sector = sector)

    arrayBaseLineValue = [float(element.value) for element in scenarioBaselineValue]

    # We add the values of the baseline across all sectors
    baselineValues[sector.full_name] = arrayBaseLineValue

  return baselineValues


def validateWhitelist(value,whitelist):
  if not clearString(value.lower()) in [clearString(s.lower()) for s in whitelist]:
    raise ValidationError(
      'Invalid value: %(value)s',
      params={'value': value},
      )


def whitelistSector(data, whitelist):
  data = data.lower()
  data = clearString(data)

  if data in [clearString(s.lower()) for s in whitelist]:
    return data
  else:
    return False


def clearString(data):
  return re.sub(r'[^\w]', '', data).lower()


def equalValues(list1, list2):
  return len(list1) == len(list2) and sum(map(eq, list1, list2)) == len(list1)

def defaul_factory():
  return "Escenario base"

url_traslate = [
    ('base' , 'Escenario base'),

    ('ernc' , 'Escenario ERNC'),
    ('nuclear' , 'Escenario nuclear'),
    ('er' , 'Escenario ER'),
    ('carbontax' , 'Escenario ERNC'),
    ('ee' , 'Escenario EE'),
    ('8020' , 'Escenario 80/20'),

    ('pibbajo' , 'LB2013 PIB bajo'),
    ('pibmedio' , 'LB2013 PIB medio'),
    ('pibalto' , 'LB2013 PIB alto'),

    ('medio' , 'Escenario medio'),
    ('alto' , 'Escenario alto'),

    ('sinmedidas' , 'Escenario base'),
  ]

url = collections.defaultdict(defaul_factory,url_traslate)
url_back = collections.defaultdict(
  lambda: 'No existe',
  {v: k for k, v in url.items()}
  )



sectorWhiteList = ['general', 'generacion', 'mineria','I&M', 'cpr',
              'transporte','residuos', 'agropecuario',
              'forestal']

scenarioWhiteList = ['LB2013 PIB medio','LB2013 PIB bajo',
              'LB2013 PIB alto', 'Escenario base', 'Escenario medio',
              'Escenario alto','Escenario ERNC','Escenario ER',
              'Escenario EE', 'Escenario 80/20', 'Escenario nuclear',
              'Escenario base']

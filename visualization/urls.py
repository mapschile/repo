from django.conf.urls import patterns, url
from django.conf import settings

from visualization import views

urlpatterns = patterns('visualization.views',
    url(r'^$', 'general.index', name='index'),

    url(r'^$', 'general.index', name='escenario'),
    #url(r'^$', 'index', name='index'),

    url(r'^actionlist$', 'general.actionListViewer', name='actionListViewer'),
    url(r'^comparison$', 'comparison.home', name='comparison'),
    url(r'^data/comparison$', 'general.dataSelector', name='dataSelectorComparison'),
    url(r'^data/scenario$', 'general.dataSelector', name='dataSelectorComparison'),
    url(r'^data/baseline$', 'general.baseline', name='databaseline'),

    url(r'^scenario/(\d+)/$', 'scenario.home', name='scenario'),
    url(r'^scenario/$', 'scenario.scenario_default', name='scenario_default'),

    url(r'^compositionData$', 'scenario.compositionData', name='dataSelectorComposition'),
    url(r'^registry$', 'registry.registry_viewer', name='registry'),
    url(r'^actionGraphComparison$', 'registry.actionGraphComparison', name='registryGraph'),
    url(r'^actionGraphReduction$', 'registry.actionGraphReduction', name='registryGraph2'),

    url(r'^personalized/$', 'personalized.scenario', name='scenario_personalized'),
    url(r'^updatePersonalizedScenario/$', 'personalized.updatePersonalizedScenario', name='updatePersonalizedScenario'),
    url(r'^updatePersonalizedScenario/$', 'personalized.updatePersonalizedScenario', name='refreshPersonalizedScenario'),

    url(r'^cleanPersonalizedScenario/$', 'personalized.cleanPersonalizedScenario', name='cleanPersonalizedScenario'),
    url(r'^test/$', 'personalized.testPersonalizedScenario', name='test'),

    url(r'^images/$', 'general.imageViewer', name='images'),

    url(r'^credits/$', 'general.credits', name='credits'),

)

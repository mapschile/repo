# -*- encoding: utf-8 -*-

from administration.models import *
from visualization.utils import getScenarios

data = {
		u"Línea Base 2013 (PIB medio)": {
				"isVisible": True,
				"enabled": True,
				"color": "#04B404",
				"marker": False,
				"lineStyle": "line"
		},
		u"Línea Base 2013 (PIB bajo)": {
				"isVisible": True,
				"enabled": False,
				"color": "#5F8034",
				"marker": False,
				"lineStyle": "line"
		},
		u"Línea Base 2013 (PIB alto)": {
				"isVisible": True,
				"enabled": False,
				"color": "#5F8034",
				"marker": False,
				"lineStyle": "line"
		},
		u"Escenario nivel de esfuerzo base": {
				"isVisible": True,
				"enabled": True,
				"color": "#5A458C",
				"marker": False,
				"lineStyle": "shortdash"
		},
		u"Escenario nivel de esfuerzo medio": {
				"isVisible": True,
				"enabled": True,
				"color": "#5A458C",
				"marker": True,
				"lineStyle": "line"
		},
		u"Escenario nivel de esfuerzo alto": {
				"isVisible": True,
				"enabled": True,
				"color": "#5A458C",
				"marker": False,
				"lineStyle": "line"
		},
		u"Escenario energías renovables no convencionales": {
				"isVisible": True,
				"enabled": True,
				"color": "#C12B2D",
				"marker": False,
				"lineStyle": "line"
		},
		u"Escenario energías renovables": {
				"isVisible": True,
				"enabled": True,
				"color": "#1565A5",
				"marker": False,
				"lineStyle": "line"
		},
		u"Escenario eficiencia energética": {
				"isVisible": True,
				"enabled": True,
				"color": "#A89CC4",
				"marker": False,
				"lineStyle": "line"
		},
		u"Escenario 80/20": {
				"isVisible": True,
				"enabled": True,
				"color": "#5DADD1",
				"marker": False,
				"lineStyle": "line"
		},
		u"Escenario nuclear": {
				"isVisible": False,
				"enabled": True,
				"color": "#FFFFFF",
				"marker": False,
				"lineStyle": "line"
		},
		u"Escenario impuesto al carbono": {
				"isVisible": True,
				"enabled": True,
				"color": "#8776AA",
				"marker": False,
				"lineStyle": "line"
		}
}

def loadScenarioStyles(scenario):
	try:
		style = ScenarioStyle.objects.filter(scenario=scenario)[0]
	except IndexError as e:
		style = ScenarioStyle.getDefaultScenarioStyle(scenario)
	if scenario.name in data:
		style.isVisible = data[scenario.name]["isVisible"]
		style.color = data[scenario.name]["color"]

		style.marker = bool(data[scenario.name]["marker"])
		style.lineStyle = data[scenario.name]["lineStyle"]
		style.enabled = data[scenario.name]["enabled"]
	style.save()
